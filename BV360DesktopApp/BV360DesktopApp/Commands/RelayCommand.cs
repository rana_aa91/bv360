﻿using System;
using System.Windows.Input;

namespace BV360DesktopApp
{ 

    public class RelayCommand : ICommand
    {
        #region Fields

        private readonly Action execute;

     
        private readonly Func<bool> canExecute;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the RelayCommand class
        /// </summary>
        /// <param name="execute">command action</param>
        public RelayCommand(Action execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the RelayCommand class
        /// </summary>
        /// <param name="execute">command action</param>
        /// <param name="canExecute">CanExecute action</param>
        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }

            this.execute = execute;
            this.canExecute = canExecute;
        }

        #endregion

        #region ICommand Members

        /// <summary>
        /// Event handler for CanExecuteChanged
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (this.canExecute != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }

            remove
            {
                if (this.canExecute != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }

        /// <summary>
        /// Function to execute CanExecute action and return the boolean result.
        /// If action for canexecute is not defined then it will return true.
        /// </summary>
        /// <param name="parameter">action parameter</param>
        /// <returns>true if execution is allowed otherwise false</returns>
        public bool CanExecute(object parameter)
        {
            return this.canExecute == null ? true : this.canExecute();
        }        

        /// <summary>
        /// Function to execute action of command.
        /// </summary>
        /// <param name="parameter">action parameter</param>
        public void Execute(object parameter)
        {
            this.execute();
        }

        #endregion        
    }
}