﻿using System;
using System.Windows.Input;

namespace BV360DesktopApp
{


    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    /// <typeparam name="T">type of command action parameter</typeparam>
    public class RelayCommand<T> : ICommand
    {
        #region Fields

        /// <summary>
        /// Holds action for command
        /// </summary>
        private readonly Action<T> executeGeneric = null;

        /// <summary>
        /// Holds command CanExecute action
        /// </summary>
        private readonly Predicate<T> canExecuteGeneric = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the RelayCommand class
        /// </summary>
        /// <param name="execute">command action</param>
        public RelayCommand(Action<T> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the RelayCommand class
        /// </summary>
        /// <param name="executeGeneric">command action</param>
        /// <param name="canExecuteGeneric">CanExecute action</param>
        public RelayCommand(Action<T> executeGeneric, Predicate<T> canExecuteGeneric)
        {
            if (executeGeneric == null)
            {
                throw new ArgumentNullException("executeGeneric");
            }

            this.executeGeneric = executeGeneric;
            this.canExecuteGeneric = canExecuteGeneric;
        }

        #endregion

        #region ICommand Members        

        /// <summary>
        /// Event handler for CanExecuteChanged
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (this.canExecuteGeneric != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }

            remove
            {
                if (this.canExecuteGeneric != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }

        /// <summary>
        /// Function to execute CanExecute action and return the boolean result.
        /// If action for canexecute is not defined then it will return true.
        /// </summary>
        /// <param name="parameter">action parameter</param>
        /// <returns>true if execution is allowed otherwise false</returns>
        public bool CanExecute(object parameter)
        {
            try
            {
                return this.canExecuteGeneric == null ? true : this.canExecuteGeneric((T)parameter);
            }
            catch (InvalidCastException exception)
            {
                exception.Log();                
            }

            return false;
        }

        /// <summary>
        /// Function to execute action of command.
        /// </summary>
        /// <param name="parameter">action parameter</param>
        public void Execute(object parameter)
        {
            try
            {
                this.executeGeneric((T)parameter);
            }
            catch (InvalidCastException exception)
            {
                exception.Log();
            }
        }

        #endregion        
    }    
}