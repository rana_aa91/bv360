﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using log4net;

namespace BV360DesktopApp
{
    public class MainViewModel : ViewModelBase
    {
        #region Private Fields

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int HeaderHeightForRepositioningWindow = 60;

        private UploadHandler h;

        private UploadSession uploadSession = new UploadSession();

        private SiteItem site = new SiteItem();

        private string videoDirectory;
        /// <summary>
        /// Holds path of directory which holds the original and compressed media file.
        /// </summary>
        private string mainDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BV360Directory");

        /// <summary>
        /// Holds path of directory which holds compressed media file.
        /// </summary>
        private string compressedFileDirectory = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BV360Directory"), "CompressedFiles");

        #endregion

        #region Public Fields

        public ICommand RunCommand
        {
            get { return new RelayCommand(this.Run, this.CanRun); }
        }

        public ICommand CloseCommand
        {
            get { return new RelayCommand(this.CloseWindow); }
        }

        public ICommand SelectFilesCommand
        {
            get { return new RelayCommand(this.SelectFiles); }
        }

        public ICommand PlayCommand
        {
            get { return new RelayCommand<FileItem>(this.PlayVideo); }
        }

        public ICommand WindowLoadedCommand
        {
            get { return new RelayCommand(this.OnWindowLoaded); }
        }

        public ICommand SiteChangedCommand
        {
            get { return new RelayCommand(this.InitializeForNewSite); }
        }

        public UploadSession UploadSession
        {
            get { return this.uploadSession; }
            set { SetProperty(() => this.UploadSession, () => this.uploadSession, value); }
        }

        public string VideoDirectory
        {
            get { return this.videoDirectory; }
            set { SetProperty(() => this.VideoDirectory, () => this.videoDirectory, value); }
        }

        public IList<SiteItem> SiteList { get; set; }

        public SiteItem Site
        {
            get { return this.site; }
            set { SetProperty(() => this.Site, () => this.site, value); }
        }
        #endregion

        #region Public Methods

        public void LoadFileList(IList<FileItem> files)
        {
            this.UploadSession.FileList.Clear();
            this.UploadSession.Site = this.Site;
            files.Select(f => { this.UploadSession.FileList.Add(f); return true; }).ToList();

            CheckAlreadyExistingFiles();
        }

        public void LoadSiteList()
        {
            //sites are hard-coded for now
            //TODO: need to call this list from through API
            SiteItem etp = new SiteItem
            {
                SiteId = "47cfc0e2-aa80-479e-8cff-e90a06d802a8",
                SiteName = "Engineering Technology Precinct ",
                Shorthand = "etp"
            };
            SiteItem enexg = new SiteItem
            {
                SiteId = "aa523604-8b1a-4981-bfc7-122f1a830ee7",
                SiteName = "Engineering Excellence",
                Shorthand = "enexg"
            };
            SiteItem csm = new SiteItem
            {
                SiteId = "f68e2a5e-1c3e-45ed-972e-74924dbe3542",
                SiteName = "Central Station Metro",
                Shorthand = "csm"
            };

            this.SiteList = new List<SiteItem>();

            SiteList.Add(etp);
            SiteList.Add(enexg);
            SiteList.Add(csm);

            this.Site = this.SiteList.FirstOrDefault();

        }

        public void OnWindowLoaded()
        {
            //no need for now
            // this.InitializeforNewSite();
        }

        private void InitializeForNewSite()
        {
            //not used for now
        }

        private async void CheckAlreadyExistingFiles()
        {
            foreach (FileItem f in this.UploadSession.FileList)
            {
                if (await HashHandler.HashExists(f.HashString))
                    f.FileStatus = FileStatus.AlreadyExists;
            }
        }

        public MainViewModel()
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Loading Site List");
            this.LoadSiteList();
        }

        #endregion

        #region Private Methods


        /// <summary>
        /// Function to execute command in command prompt.
        /// </summary>
        /// <param name="command">Command to be executed.</param>
        private static void ExecuteCommandSync(object command)
        {
            // create the ProcessStartInfo using "cmd" as the program to be run,
            // and "/c " as the parameters.
            // Incidentally, /c tells cmd that we want it to execute the command that follows,
            // and then exit.
            System.Diagnostics.ProcessStartInfo procStartInfo =
                new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

            // The following commands are needed to redirect the standard output.
            // This means that it will be redirected to the Process.StandardOutput StreamReader.
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;
            // Do not create the black window.
            procStartInfo.CreateNoWindow = true;

            // Now we create a process, assign its ProcessStartInfo and start it
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = procStartInfo;
            proc.Start();
            // Get the output into a string
            string result = proc.StandardOutput.ReadToEnd();
        }

        private async Task RunCompressFile(FileItem f)
        {
            try
            {
                f.FileStatus = FileStatus.CompressionInProgress;
                await CompressFile(f);
            }
            catch
            {
                log.Error("Failed to compress FileItem " + f.Name);
            }
        }

        private async Task CompressFile(FileItem fileDetail)
        {
            if (!Directory.Exists(this.mainDirectory))
                Directory.CreateDirectory(this.mainDirectory);
            if (!Directory.Exists(this.compressedFileDirectory))
                Directory.CreateDirectory(this.compressedFileDirectory);

            var tempFilePath = Path.Combine(this.compressedFileDirectory, fileDetail.Name);

            log.Debug("Compressing to " + tempFilePath);

            await Task.Run(() =>
            {
                var command = AppDomain.CurrentDomain.BaseDirectory + "ffmpeg\\ffmpeg.exe -i " + fileDetail.Path + " -crf 24 -preset ultrafast " + tempFilePath;
                log.Info("Running: " + command.ToString());
                ExecuteCommandSync(command);
            });

            if(File.Exists(tempFilePath))
            {
                fileDetail.Path = tempFilePath;
                log.Debug("Changed file path to " + tempFilePath);
            }
                
        }

        private void DeleteFileCompressionDirectory()
        {
            if (Directory.Exists(this.mainDirectory))
            {
                Directory.Delete(this.mainDirectory, true);
            }
        }

        private void OnErrorFileUploading(FileItem fileDetail)
        {
            ApplicationDispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() =>
            {
                log.Error($"{fileDetail.Name} failed to upload.");
                fileDetail.FileStatus = FileStatus.Error;
                RefreshCommand();
            }));
        }

        private static void RefreshCommand()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        private void SelectFiles()
        {
            if (this.UploadSession.FileList != null) //Q: when could you select with FileList null ?
            {
                this.UploadSession.FileList.Select(f => { f.IsSelected = true; return true; }).ToList();
            }
        }

        /// <summary>
        /// Function to start video upload operation.
        /// </summary>
        private async void Run()
        {
            //TODO: 
            //Step1. Check files against those already on the server
            //Step2. Remove any matching files
            RemoveUnselectedFiles();
            //Step3: Compress & Upload processed files to server
            //this.UploadSession.StartDateTime = DateTime.Now;
            await UploadFiles();
            //Step3: Delete files in temp folder

        }

        private void RemoveUnselectedFiles()
        {
            var unSelectedFiles = this.UploadSession.FileList.Where(f => !f.IsSelected).ToList();
            unSelectedFiles.Select(f => { this.UploadSession.FileList.Remove(f); return true; }).ToList();

            // Updates file indices
            int count = 1;
            this.UploadSession.FileList.Select(f => { f.Index = count; count++; return true; }).ToList();
        }

        private async Task UploadFiles()
        {
            // Clear all error messages.
            this.UploadSession.FileList.Select(f => { f.FileStatus = FileStatus.None; return true; }).ToList();

            // Start "progress indicator" for selected files.
            this.UploadSession.FileList.Select(f => { f.FileStatus = FileStatus.Pending; return true; }).ToList();

            // process selected files.
            int count = 0;
            log.Info($"{this.UploadSession.FileList.Count.ToString()} files selected for upload to site {this.UploadSession.Site.SiteName}.");
            foreach (var f in this.UploadSession.FileList)
            {
                if (f.CompressSelected)
                    await RunCompressFile(f);
                log.Info($"Starting Upload Process for {f.Name}");
                UploadFile(f, count);
                count++;
            }
        }

        private async void UploadFile(FileItem f, int count)
        {
            try
            {
                f.FileStatus = FileStatus.Setup;
                log.Info($"Starting upload setup for {f.Name}.");
                h = new UploadHandler(f, this.Site);
                if (!(await h.UploadSetup()))
                {
                    log.Info($"Upload Setup for {f.Name} failed.");
                    this.OnErrorFileUploading(f);
                    return;
                }
                log.Info($"Upload setup for {f.Name} succeeded");
                log.Info($"Processing {h.TotalChunkNum.ToString()} chunks for {f.Name}");
                f.FileStatus = FileStatus.UploadInProgress;
                for (int i = 0; i < h.TotalChunkNum; i++)
                {
                    if (!(await h.UploadNextChunkAsync()))
                    {
                        log.Debug($"Uploading chunk #{i.ToString()}/{h.TotalChunkNum.ToString()} failed for {f.Name}");
                        this.OnErrorFileUploading(f);
                        return;
                    }                    
                    this.UploadSession.FileList[count].ChunkPercentageInProgress = ((int)(100.0 * (i + 1) / h.TotalChunkNum)).ToString() + " %";
                }
                if (await h.UploadFinishAsync())
                {
                    log.Info($"Finish upload command succeeded for {f.Name}");
                    this.OnUploadingCompletion(f);
                }
                else
                {
                    log.Error($"Finish upload command failed for {f.Name}");
                    this.OnErrorFileUploading(f);
                }

            }
            catch (Exception ex)
            {
                log.Error($"Finish upload command failed for {f.Name} with exception {ex.Message}");
                this.OnErrorFileUploading(f);
                
            }
        }

        private void OnUploadingCompletion(FileItem fileDetail)
        {
            fileDetail.FileStatus = FileStatus.Complete;
            CommandManager.InvalidateRequerySuggested();
        }

        private void PlayVideo(FileItem fileDetail)
        {
            Messenger.Instance.Notify(MessengerMessage.OpenVideoPreviewView, fileDetail);
        }

        private void CloseWindow()
        {
            Messenger.Instance.Notify(MessengerMessage.CloseApplication);

        }



        /// <summary>
        /// Function to check whether user can start uploading (enables Run button, otherwise grey).
        /// </summary>
        /// <returns>True or false</returns>
        private bool CanRun()
        {
            return this.UploadSession.FileList != null && this.UploadSession.FileList.Count > 0 && this.UploadSession.FileList.Any(f => f.IsSelected) && this.UploadSession.FileList.All(f => f.FileStatus != FileStatus.UploadInProgress);
        }
        #endregion
    }
}