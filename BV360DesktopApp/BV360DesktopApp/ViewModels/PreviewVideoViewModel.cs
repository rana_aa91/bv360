﻿using System.Collections.Generic;
using System.Windows.Input;

namespace BV360DesktopApp
{
   
    public class PreviewVideoViewModel : ViewModelBase
    {

        private FileItem fileDetail;

        private string descripton;
        private IList<string> tags = new List<string>();

        private string DescriptionBox = ""; //empty string

        public FileItem FileDetail
        {
            get { return this.fileDetail; }
            private set { SetProperty(() => this.FileDetail, () => this.fileDetail, value); }
        }

        public IList<string> Tags
        {
            get { return this.tags; }
            private set { SetProperty(() => this.Tags, () => this.tags, value); }
        }         
       
        public string Description { get; set; }
               
        public ICommand SaveTagsCommand
        {
            get { return new RelayCommand(this.SaveTags); }
        }

        public ICommand SaveDescriptionCommand
        {
            get { return new RelayCommand(this.SaveDescription); }
        }

        public void AssignFilePath(FileItem fileDetail)
        {
            this.FileDetail = fileDetail;
        }
    
        private void SaveTags()
        {
            Messenger.Instance.Notify(MessengerMessage.SaveTags);
        }

        private void SaveDescription()
        {
            Messenger.Instance.Notify(MessengerMessage.SaveDescription);
        }

    }
}