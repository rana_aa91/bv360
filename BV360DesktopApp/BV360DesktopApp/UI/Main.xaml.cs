﻿
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using System.IO;
using System;
using log4net;
using log4net.Repository;
using log4net.Appender;

namespace BV360DesktopApp
{
    public partial class Main : Window
    {      
        //log manager
        private static string logFileLocation = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        private static string logFileName = Path.Combine(logFileLocation, "BV360_" + DateTime.Now.ToString());
        //private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log = LogManager.GetLogger(logFileName);

        private MainViewModel mainViewModel = new MainViewModel();

        public Main()
        {
            log4net.Config.XmlConfigurator.Configure();

            InitializeComponent();         

            this.DataContext = this.mainViewModel; //things to display
            this.RegisterEvents();

            //Ffmpeg.Compress();

            //Retrieve user settings e.g. site name,video directory ?
            
            //TODO: retrieve hashes and tags from website ?
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {


        }


        private void SourceButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                log.Info("Browse Button Selected");
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
               
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    // Gets the selected directory name and displays in a TextBox.
                    this.sourceDirectoryTextBlock.Text = dialog.SelectedPath;
                    this.mainViewModel.VideoDirectory = dialog.SelectedPath;
                    log.Info($"Directory Selected: {dialog.SelectedPath}");
                    log.Info("Loading Files...");
                    this.LoadFileList();                    
                }
            }

        }


        private void RegisterEvents()
        {
            Messenger.Instance.Register<FileItem>(MessengerMessage.OpenVideoPreviewView, this.OpenVideoPreviewView);
            Messenger.Instance.Register(MessengerMessage.CloseApplication, this.Close);

        }

        private void OpenVideoPreviewView(FileItem fileDetail)
        {
            System.Windows.Application.Current.ShowWindowAsDialog<PreviewVideo>(() => new PreviewVideo(fileDetail));
            log.Info($"Preview Button Opened for {fileDetail.Name}");
        }

        private void LoadFileList()
        {
            string[] files = System.IO.Directory.GetFiles(this.sourceDirectoryTextBlock.Text, "*.mp4");

            IList<FileItem> fileDetailList = new List<FileItem>();
            int index = 1;
            foreach (var f in files)
            {
                FileInfo fileInfo = new FileInfo(f);

                FileItem fileDetail = new FileItem()
                {
                    Index = index,
                    Name = fileInfo.Name,
                    Path = f,
                    Length = fileInfo.Length,
                    FileExtension = fileInfo.Extension,
                    CreationDateTime = fileInfo.CreationTime,
                };

                fileDetailList.Add(fileDetail);
                index++;
            }
            log.Info($"Total of {index} mp4 files found.");
            this.mainViewModel.LoadFileList(fileDetailList);
        }

        
    }
}






