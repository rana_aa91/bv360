﻿// <copyright file="PreviewVideo.xaml.cs" company="">Copyright (C) 2020</copyright>
using System;
using System.Windows;
using System.Windows.Input;
using System.Linq;

namespace BV360DesktopApp
{

    public partial class PreviewVideo : Window
    {

        private const int HeaderHeightForRepositioningWindow = 60;
        private PreviewVideoViewModel previewVideoViewModel = new PreviewVideoViewModel();
        private FileItem fileDetail;


        public PreviewVideo(FileItem fileDetail)
        {           
            this.InitializeComponent();
            this.DataContext = this.previewVideoViewModel;
            this.previewVideoViewModel.AssignFilePath(fileDetail);
            this.fileDetail = fileDetail;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.RegisterEvent();
            this.FillDetails();
        }

        private void SeekToMediaPosition(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int SliderValue = (int)timelineSlider.Value;

            // Overloaded constructor takes the arguments days, hours, minutes, seconds, milliseconds.
            // Creates a TimeSpan with miliseconds equal to the slider value.
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);
            videoPreviewMediaElement.Position = ts;
        }

        private void Element_MediaOpened(object sender, EventArgs e)
        {
            timelineSlider.Maximum = videoPreviewMediaElement.NaturalDuration.TimeSpan.TotalMilliseconds;
        }

   
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            this.UnregisterEvent();
        }

        private void RegisterEvent()
        {
            Messenger.Instance.Register(MessengerMessage.SaveTags, this.SaveTags);
        }

        private void UnregisterEvent()
        {
            Messenger.Instance.Unregister(MessengerMessage.SaveTags);
        }

        private void SaveTags()
        {
            this.fileDetail.Tags.Clear();
            if (this.videoTagControl.Items != null && this.videoTagControl.Items.Count > 0)
            {
                
                foreach (var tagItem in this.videoTagControl.Items)
                {
                    if (tagItem is VideoTagItem item)
                    {
                        this.fileDetail.Tags.Add(item.Text);
                    }
                }
            }

            if(DescriptionBox.Text != "")
            {
                this.fileDetail.Description = DescriptionBox.Text;
            }

            if(FileCompressCheckBox.IsChecked.HasValue)
                this.fileDetail.CompressSelected = (bool)FileCompressCheckBox.IsChecked;
            this.Close();
        }

        private void OnWindowMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Following code reposition the window dragging at header of the window.
            if (e.GetPosition(this).Y < HeaderHeightForRepositioningWindow)
            {
                if (e.ClickCount == 1)
                {
                    // Mouse is single clicked.
                    this.DragMove();
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void FillDetails()
        {
            this.fileDetail.Tags.Select(t => { this.videoTagControl.AddTag(new VideoTagItem() { Text = t }); return true; }).ToList();
        }
    }
}