﻿using PortableDevices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BV360DesktopApp.CameraDetector
{
    class Insta360Detector
    {
        public delegate void FoundCameraEventHandler(object source, EventArgs args);
        public event FoundCameraEventHandler FoundCamera;

        public ManagementEventWatcher insertWatcher;
        public ManagementEventWatcher removeWatcher;

        private const string CameraID = "USB\\VID_4255&PID_0001\\IXE5018NEGWJ3S";
        private const string CameraIDLabel = "DeviceID";

        private string cameraPath;

        private void FindCamera()
        {
            Console.WriteLine("Looking for Camera...");
            Thread.Sleep(3000);

            OnFoundCamera();

        }

        protected virtual void OnFoundCamera()
        {
            FoundCamera?.Invoke(this, EventArgs.Empty);
        }

        private void DeviceInsertedEvent(object sender, EventArrivedEventArgs e)
        {
            ManagementBaseObject instance = (ManagementBaseObject)e.NewEvent["TargetInstance"];
            foreach (PropertyData prop in instance.Properties)
                Console.WriteLine(prop.Name + " " + prop.Value);
            if (IsDeviceCamera(instance))
                cameraPath = GetCameraPath(instance);
            //StartSync();
        }

        private void DeviceRemovedEvent(object sender, EventArrivedEventArgs e)
        {
            ManagementBaseObject instance = (ManagementBaseObject)e.NewEvent["TargetInstance"];
            foreach (PropertyData prop in instance.Properties)
                Console.WriteLine(prop.Name + " " + prop.Value);
            if (IsDeviceCamera(instance))
            {
                //pause or StopSync();
            }

        }

        private bool IsDeviceCamera(ManagementBaseObject instance)
        {
            PropertyDataCollection properties = instance.Properties;
            var hasDeviceID = (from PropertyData property in properties
                               where property.Name == CameraIDLabel
                               select property.Value).Any();
            if (hasDeviceID)
            {

                return (string)instance[CameraIDLabel] == CameraID;
            }
            return false;
        }

        private void BackgroundWorker1_DoWork(object sender, EventArgs e)
        {
            WqlEventQuery insertQuery = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_USBHub'");

            insertWatcher = new ManagementEventWatcher(insertQuery);
            insertWatcher.EventArrived += new EventArrivedEventHandler(DeviceInsertedEvent);
            insertWatcher.Start();

            WqlEventQuery removeQuery = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_USBHub'");
            removeWatcher = new ManagementEventWatcher(removeQuery);
            removeWatcher.EventArrived += new EventArrivedEventHandler(DeviceRemovedEvent);
            removeWatcher.Start();

            // Do something while waiting for events
            //System.Threading.Thread.Sleep(20000000);
        }

        private string GetCameraPath(ManagementBaseObject c)
        {
            return (string)c["DeviceID"];
        }

        public void ListDevices()
        {
            var devices = new PortableDeviceCollection();
            devices.Refresh();

            foreach (var device in devices)
            {
                device.Connect();
                string id = (string)device.DeviceId;
                var contents = device.GetContents();
                Console.WriteLine(@"DeviceId: {0}, FriendlyName: {1}", device.DeviceId, device.FriendlyName);
                device.Disconnect();
            }
        }
    }
}
