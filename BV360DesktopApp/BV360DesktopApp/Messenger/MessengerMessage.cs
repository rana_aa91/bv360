﻿// <copyright file="MessengerMessage.cs" company="">Copyright (C) 2020</copyright>

namespace BV360DesktopApp
{
    #region Namespaces
    using System;
    #endregion

    /// <summary>
    /// This class manages read only strings to be used in messaging events.
    /// </summary>
    public static class MessengerMessage
    {
        #region Public Members

        /// <summary>
        /// Read-Only string for "Open Video Preview View" message.
        /// </summary>
        public static readonly string OpenVideoPreviewView = "BV360DesktopApp.OpenVideoPreviewView";

        /// <summary>
        /// Read-Only string for "Close Application" message.
        /// </summary>
        public static readonly string CloseApplication = "BV360DesktopApp.CloseApplication";

        /// <summary>
        /// Read-Only string for "Save Tags" message.
        /// </summary>
        public static readonly string SaveTags = "PreviewVideoViewModel.SaveTags";

        /// <summary>
        /// Read-Only string for "Save Description" message.
        /// </summary>
        public static readonly string SaveDescription = "PreviewVideoViewModel.SaveDescription";
        #endregion
    }
}