﻿// <copyright file="Messenger.cs" company="">Copyright (C) 2020</copyright>

namespace BV360DesktopApp
{
    #region Namespace
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Reflection;
    using System.Threading.Tasks;
    #endregion

    /// <summary>
    /// This class contains functionality to provide loosely-coupled 
    /// messaging between various colleague objects. All references 
    /// to objects are stored weakly to prevent any 
    /// memory leakage.
    /// </summary>
    public class Messenger
    {
        #region Fields

        /// <summary>
        /// Holds list of message and callback actions
        /// </summary>
        private readonly MessageToActionsMap messageToActionsMap = new MessageToActionsMap();

        /// <summary>
        /// Holds instance of messenger
        /// </summary>
        private static Messenger instance;

        #endregion

        #region Public Fields

        /// <summary>
        /// Gets an instance of messenger
        /// </summary>
        public static Messenger Instance
        {
            get
            {
                instance = instance ?? new Messenger();
                return instance;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Function to register a callback method, with no parameter, to be invoked when a specific message is broadcasted.
        /// </summary>
        /// <param name="message">The message to register for.</param>
        /// <param name="callback">The callback to be called when this message is broadcasted.</param>
        public void Register(string message, Action callback)
        {
            this.Register(message, callback, null);
        }

        /// <summary>
        /// Function to register a callback method, with a parameter, to be invoked when a specific message is broadcasted.
        /// </summary>
        /// <param name="message">The message to register for.</param>
        /// <param name="callback">The callback to be called when this message is broadcasted.</param>
        /// <typeparam name="T">type of parameter in callback</typeparam>
        public void Register<T>(string message, Action<T> callback)
        {
            this.Register(message, callback, typeof(T));
        }

        /// <summary>
        /// Function to notify all registered parties that a message is being broadcasted.
        /// </summary>
        /// <param name="message">The message to broadcast.</param>
        /// <param name="parameter">The parameter to pass together with the message.</param>
        public void Notify(string message, object parameter)
        {
            if (String.IsNullOrEmpty(message))
            {
                throw new ArgumentException("'message' cannot be null or empty.");
            }

            Type registeredParameterType;
            if (this.messageToActionsMap.TryGetParameterType(message, out registeredParameterType))
            {
                if (registeredParameterType == null)
                {
                    throw new TargetParameterCountException(string.Format(
                        CultureInfo.CurrentCulture,
                        "Cannot pass a parameter with message '{0}'. Registered action(s) expect no parameter.", 
                        message));
                }
            }

            var actions = this.messageToActionsMap.GetActions(message);
            if (actions != null)
            {
                actions.ForEach(action => action.DynamicInvoke(parameter));
            }
        }

        /// <summary>
        /// Function to notify all registered parties that a message is being broadcasted.
        /// </summary>
        /// <param name="message">The message to broadcast.</param>
        public void Notify(string message)
        {
            if (String.IsNullOrEmpty(message))
            {
                throw new ArgumentException("'message' cannot be null or empty.");
            }

            Type registeredParameterType;
            if (this.messageToActionsMap.TryGetParameterType(message, out registeredParameterType))
            {
                if (registeredParameterType != null)
                {
                    throw new TargetParameterCountException(string.Format(
                        CultureInfo.CurrentCulture,
                        "Must pass a parameter of type {0} with this message. Registered action(s) expect it.",
                        registeredParameterType.FullName));
                }
            }

            var actions = this.messageToActionsMap.GetActions(message);
            if (actions != null)
            {
                actions.ForEach(action => action.DynamicInvoke());
            }
        }

        /// <summary>
        /// Function to un-register message from message list.
        /// </summary>
        /// <param name="message">The message to be unregistered</param>
        public void Unregister(string message)
        {
            this.messageToActionsMap.RemoveActions(message);
        }

        /// <summary>
        /// Function to find out whether message is registered.
        /// </summary>
        /// <param name="message">Message to be checked</param>
        /// <returns>True or false</returns>
        public bool IsRegistered(string message)
        {
            return this.messageToActionsMap.IsMessageExist(message);
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Function to register a callback method, with a parameter, to be invoked when a specific message is broadcasted.
        /// </summary>
        /// <param name="message">The message to register for.</param>
        /// <param name="callback">The callback to be called when this message is broadcasted.</param>
        /// <param name="parameterType">The parameter type of callback.</param>
        private void Register(string message, Delegate callback, Type parameterType)
        {
            if (String.IsNullOrEmpty(message))
            {
                throw new ArgumentException("'message' cannot be null or empty.");
            }

            if (callback == null)
            {
                throw new ArgumentNullException("callback");
            }

            this.VerifyParameterType(message, parameterType);

            this.messageToActionsMap.AddAction(message, callback.Target, callback.Method, parameterType);
        }

        /// <summary>
        /// Function to verify parameter type
        /// </summary>
        /// <param name="message">The message to register for.</param>
        /// <param name="parameterType">The parameter type of callback.</param>
        [Conditional("DEBUG")]
        private void VerifyParameterType(string message, Type parameterType)
        {
            Type previouslyRegisteredParameterType = null;
            if (this.messageToActionsMap.TryGetParameterType(message, out previouslyRegisteredParameterType))
            {
                if (previouslyRegisteredParameterType != null && parameterType != null)
                {
                    if (!previouslyRegisteredParameterType.Equals(parameterType))
                    {
                        throw new InvalidOperationException(string.Format(
                            CultureInfo.CurrentCulture,
                            "The registered action's parameter type is inconsistent with the previously registered actions for message '{0}'.\nExpected: {1}\nAdding: {2}",
                            message,
                            previouslyRegisteredParameterType.FullName,
                            parameterType.FullName));
                    }
                }
                else
                {
                    // One, or both, of previouslyRegisteredParameterType or callbackParameterType are null.
                    if (previouslyRegisteredParameterType != parameterType)   
                    {
                        throw new TargetParameterCountException(string.Format(
                            CultureInfo.CurrentCulture,
                            "The registered action has a number of parameters inconsistent with the previously registered actions for message \"{0}\".\nExpected: {1}\nAdding: {2}",
                            message,
                            previouslyRegisteredParameterType == null ? 0 : 1,
                            parameterType == null ? 0 : 1));
                    }
                }
            }
        }        

        #endregion

        #region MessageToActionsMap [nested class]

        /// <summary>
        /// This class is an implementation detail of the Messenger class.
        /// </summary>
        private class MessageToActionsMap
        {
            #region Fields

            /// <summary>
            /// Stores a hash where key is the message 
            /// and value is the list of callbacks 
            /// which are to be invoked.
            /// </summary>
            private readonly Dictionary<string, List<WeakAction>> map = new Dictionary<string, List<WeakAction>>();

            #endregion // Fields

            #region AddAction

            /// <summary>
            /// Adds an action to the list.
            /// </summary>
            /// <param name="message">The message to register.</param>
            /// <param name="target">The target object to invoke, or null.</param>
            /// <param name="method">The method to invoke.</param>
            /// <param name="actionType">The type of the Action delegate.</param>
            internal void AddAction(string message, object target, MethodInfo method, Type actionType)
            {
                if (message == null)
                {
                    throw new ArgumentNullException("message");
                }

                if (method == null)
                {
                    throw new ArgumentNullException("method");
                }

                lock (this.map)
                {
                    if (!this.map.ContainsKey(message))
                    {
                        this.map[message] = new List<WeakAction>();
                    }

                    this.map[message].Add(new WeakAction(target, method, actionType));
                }
            }
            #endregion // AddAction

            #region RemoveActions
            /// <summary>
            /// Function to remove actions to be invoked for the specified message.
            /// </summary>
            /// <param name="message">The message to remove the actions for.</param>
            internal void RemoveActions(string message)
            {
                if (message == null)
                {
                    throw new ArgumentNullException("message");
                }

                lock (this.map)
                {
                    if (!this.map.ContainsKey(message))
                    {
                        return;
                    }

                    this.map.Remove(message);
                }
            }

            /// <summary>
            /// Function to find out whether message exists.
            /// </summary>
            /// <param name="message">The message to be checked</param>
            /// <returns>True or false</returns>
            internal bool IsMessageExist(string message)
            {
                if (message == null)
                {
                    throw new ArgumentNullException("message");
                }

                lock (this.map)
                {
                    return this.map.ContainsKey(message);
                }
            }
            #endregion

            #region GetActions

            /// <summary>
            /// Gets the list of actions to be invoked for the specified message
            /// </summary>
            /// <param name="message">The message to get the actions for</param>
            /// <returns>Returns a list of actions that are registered to the specified message</returns>
            internal List<Delegate> GetActions(string message)
            {
                if (message == null)
                {
                    throw new ArgumentNullException("message");
                }

                List<Delegate> actions;
                lock (this.map)
                {
                    if (!this.map.ContainsKey(message))
                    {
                        return null;
                    }

                    List<WeakAction> weakActions = this.map[message];
                    actions = new List<Delegate>(weakActions.Count);
                    for (int i = weakActions.Count - 1; i > -1; --i)
                    {
                        WeakAction weakAction = weakActions[i];
                        if (weakAction == null)
                        {
                            continue;
                        }

                        Delegate action = weakAction.CreateAction();
                        if (action != null)
                        {
                            actions.Add(action);
                        }
                        else
                        {
                            // Gets rid of the weak action because the target object is dead.
                            weakActions.Remove(weakAction);
                        }
                    }

                    // Deletes the list from the map if it is now empty.
                    if (weakActions.Count == 0)
                    {
                        this.map.Remove(message);
                    }
                }

                // Reverses the list to ensure the callbacks are invoked in the order they were registered.
                actions.Reverse();

                return actions;
            }

            #endregion // GetActions

            #region TryGetParameterType

            /// <summary>
            /// Gets the parameter type of the actions registered for the specified message.
            /// </summary>
            /// <param name="message">The message to check for actions.</param>
            /// <param name="parameterType">
            /// When this method returns, contains the type for parameters 
            /// for the registered actions associated with the specified message, if any; otherwise, null.
            /// This will also be null if the registered actions have no parameters.
            /// This parameter is passed un-initialized.
            /// </param>
            /// <returns>true if any actions were registered for the message</returns>
            internal bool TryGetParameterType(string message, out Type parameterType)
            {
                if (message == null)
                {
                    throw new ArgumentNullException("message");
                }

                parameterType = null;
                List<WeakAction> weakActions;
                lock (this.map)
                {
                    if (!this.map.TryGetValue(message, out weakActions) || weakActions.Count == 0)
                    {
                        return false;
                    }
                }

                parameterType = weakActions[0].ParameterType;
                return true;
            }

            #endregion
        }

        #endregion

        #region WeakAction [nested class]

        /// <summary>
        /// This class is an implementation detail of the MessageToActionsMap class.
        /// </summary>
        private class WeakAction
        {
            #region Fields

            /// <summary>
            /// Holds parameter type
            /// </summary>
            internal readonly Type ParameterType;

            /// <summary>
            /// Holds delegate type
            /// </summary>
            private readonly Type delegateType;

            /// <summary>
            /// Holds method information
            /// </summary>
            private readonly MethodInfo method;

            /// <summary>
            /// Holds target reference
            /// </summary>
            private readonly WeakReference targetRef;

            #endregion

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the WeakAction class.
            /// </summary>
            /// <param name="target">The object on which the target method is invoked, or null if the method is static.</param>
            /// <param name="method">The MethodInfo used to create the Action.</param>
            /// <param name="parameterType">The type of parameter to be passed to the action. Pass null if there is no parameter.</param>
            internal WeakAction(object target, MethodInfo method, Type parameterType)
            {
                if (target == null)
                {
                    this.targetRef = null;
                }
                else
                {
                    this.targetRef = new WeakReference(target);
                }

                this.method = method;

                this.ParameterType = parameterType;

                if (parameterType == null)
                {
                    this.delegateType = typeof(Action);
                }
                else
                {
                    this.delegateType = typeof(Action<>).MakeGenericType(parameterType);
                }
            }

            #endregion // Constructor

            #region CreateAction

            /// <summary>
            /// Creates a "throw away" delegate to invoke the method on the target, or null if the target object is dead.
            /// </summary>
            /// <returns>object of type Delegate</returns>            
            internal Delegate CreateAction()
            {
                // Re-hydrate into a real Action object, so that the method can be invoked.
                if (this.targetRef == null)
                {
                    return Delegate.CreateDelegate(this.delegateType, this.method);
                }
                else
                {
                    using (Task<Delegate> task = new Task<Delegate>(() =>
                        {
                            object target = this.targetRef.Target;
                            if (target != null)
                            {
                                return Delegate.CreateDelegate(this.delegateType, target, this.method);
                            }

                            return null;
                        }))
                    {
                        task.RunSynchronously();
                        return task.Result;
                    }                    
                }                
            }

            #endregion            
        }

        #endregion        
    }
}