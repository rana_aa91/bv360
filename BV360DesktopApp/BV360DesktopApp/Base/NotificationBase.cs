﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;

namespace BV360DesktopApp
{

    [Serializable]
    public abstract class NotificationBase : INotifyPropertyChanged
    {
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChange(PropertyChangedEventArgs e)
        {
            this.PropertyChanged?.Invoke(this, e);
        }

        public void NotifyPropertyChange(string propertyName)
        {
            this.NotifyPropertyChange(new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Sets property through static reflection
        /// </summary>
        /// <typeparam name="TProperty">generic type</typeparam>
        /// <param name="targetProperty">public property name</param>
        /// <param name="targetField">field name backing public property</param>
        /// <param name="value">value to be set</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The warning is suppressed to make PropertyChanged Notification type safe")]
        protected void SetProperty<TProperty>(Expression<Func<TProperty>> targetProperty, Expression<Func<TProperty>> targetField, TProperty value)
        {
            if (targetProperty == null)
            {
                throw new InvalidOperationException("Target Property cannot be null.");
            }

            if (targetField == null)
            {
                throw new InvalidOperationException("Target Field cannot be null.");
            }

            LambdaExpression propertyLambda = targetProperty as LambdaExpression;
            LambdaExpression fieldLambda = targetField as LambdaExpression;

            PropertyInfo targetPropertyInfo = (PropertyInfo)((MemberExpression)propertyLambda.Body).Member;            
            FieldInfo targetFieldInfo = (FieldInfo)((MemberExpression)fieldLambda.Body).Member;

            object currentValue = targetPropertyInfo.GetValue(this, null);

            if (currentValue == null && value == null)
            {
                return;
            }

            // Cast to object for object equality check
            object objectValue = value;

            // if data type is string then trim the values
            string stringValue = value as string;

            if (stringValue != null)
            {
                objectValue = stringValue.Trim();
            }

            if (currentValue == null || !currentValue.Equals(objectValue))
            {
                targetFieldInfo.SetValue(this, objectValue);
                this.NotifyPropertyChange(targetPropertyInfo.Name);
            }
        }
    }
}