﻿namespace BV360DesktopApp
{


    public class ViewModelBase : NotificationBase
    {
        private bool busy = false;          // is app busy in processing something ?
        private string message = null;      // message to display when there is an error in loading data (or no data) 

        public bool IsBusy
        {
            get
            {
                return this.busy;
            }

            set
            {
                SetProperty(() => this.IsBusy, () => this.busy, value);
            }
        }

        public string Message
        {
            get { return this.message; }
            set { SetProperty(() => this.Message, () => this.message, value); }
        }

    }
}