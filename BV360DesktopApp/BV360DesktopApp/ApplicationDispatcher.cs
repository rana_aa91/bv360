﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace BV360DesktopApp
{

    /// <summary>
    /// This class contains properties to execute the specified delegate
    /// synchronously at the specified priority on the thread the Application
    /// Dispatcher is associated with. 
    /// </summary>
    public static class ApplicationDispatcher
    {
        // Remarks:
        // This class is used for executing delegate in UI thread instead of executing it directly using Dispatcher.BeginInvoke because "Dispatcher.BeginInvoke" does not work when code is executed 
        // from Unit Test. There are few reasons and solutions documented in some blog for issues in executing Dispatcher.<Method> from Unit Tests. However, all those solutions look complex and require 
        // changes in the code. 
        // Therefore a custom solution has been implemented for this problem. When code is executed from Unit Test Framework, "Application.Current" comes NULL, so we execute the delegate in the same thread. 
        // However, when code is executed from UI, "Application.Current" returns the instance of current application and we use its "Dispatcher" for executing delegate in UI thread.
        // Below are few blogs that address the same issue but have used different solution:

        // http://social.msdn.microsoft.com/forums/en-US/wpf/thread/bbae509e-a629-4b07-aa9e-9a0804558f44/
        // http://stackoverflow.com/questions/1106881/using-the-wpf-dispatcher-in-unit-tests
        // http://blogs.msdn.com/dancre/archive/2006/07/26/679870.aspx

        /// <summary>
        /// Executes the specified delegate asynchronously at the specified priority on the thread with which the Application Dispatcher is associated.
        /// </summary>
        /// <param name="priority">The priority, relative to the other pending operations in the Dispatcher</param>
        /// <param name="method">The delegate to a method that takes no arguments</param>
        /// <returns> An object, that can be used to interact with the delegate</returns>
        public static DispatcherOperation BeginInvoke(DispatcherPriority priority, Delegate method)
        {
            // Application.Current will be NULL in case of Unit tests.
            if (Application.Current == null && method != null)
            {
                method.DynamicInvoke();
                return null;
            }
            else
            {
                return Application.Current.Dispatcher.BeginInvoke(
                            priority,
                           method);
            }
        }

        /// <summary>
        /// Executes the specified delegate synchronously at the specified priority on the thread the Application Dispatcher is associated with.
        /// </summary>
        /// <param name="priority">The priority, relative to the other pending operations in the Dispatcher</param>
        /// <param name="method">The delegate to a method that takes no arguments</param>
        /// <param name="arg">An object to pass as an argument to the given method.</param>
        /// <returns> An object, that can be used to interact with the delegate</returns>
        public static object Invoke(DispatcherPriority priority, Delegate method, object arg)
        {
            // Application.Current will be NULL in case of Unit tests.
            if (Application.Current == null && method != null)
            {
                return method.DynamicInvoke(arg);
            }
            else
            {
                return Application.Current.Dispatcher.Invoke(
                            priority,
                           method,
                           arg);
            }
        }
    }
}
