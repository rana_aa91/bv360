﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace BV360DesktopApp
{
    /// <summary>
    /// This class contains extension methods for Task.
    /// Functionality of the class adds continue with action to the task for handling errors.
    /// </summary>
    public static class TaskExtensions
    {
        /// <summary>
        /// Function to add continue with action to the task for exception handling.
        /// </summary>
        /// <param name="task">object of type Task</param>
        /// <returns>task with error handler added</returns>
        public static Task OnErrorThrowOnDispatcher(this Task task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            task.ContinueWith(
                (t) =>
                {
                    t.Exception.ThrowOnDispatcher();
                },
                CancellationToken.None,
                TaskContinuationOptions.OnlyOnFaulted,
                TaskScheduler.Default);

            return task;
        }

        /// <summary>
        /// Function to add continue with action to the task for exception handling.
        /// </summary>
        /// <typeparam name="TResult">task result type</typeparam>
        /// <param name="task">object of type Task</param>
        /// <returns>task with error handler added</returns>
        public static Task<TResult> OnErrorThrowOnDispatcher<TResult>(this Task<TResult> task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            task.ContinueWith(
                (t) =>
                {
                    t.Exception.ThrowOnDispatcher();
                },
                CancellationToken.None,
                TaskContinuationOptions.OnlyOnFaulted,
                TaskScheduler.Default);

            return task;
        }
    }
}