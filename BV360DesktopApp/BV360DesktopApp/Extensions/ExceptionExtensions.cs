﻿using System;
using System.ServiceModel;
using System.Windows.Threading;

namespace BV360DesktopApp
{
    /// <summary>
    /// This class contains extension methods for Exception Type.
    /// </summary>
    public static class ExceptionExtensions
    {
        #region Public Methods

        /// <summary>
        /// Function to throw exception on dispatcher.
        /// </summary>
        /// <param name="exception">Exception information</param>
        public static void ThrowOnDispatcher(this Exception exception)
        {
            if (exception is System.ServiceModel.CommunicationException)
            {
                Log(exception);
            }
            else
            {
                // Dispatches the exception back to the main UI thread.
                // Then, again raises the exception on the main UI thread and handles it
                // from the handler of the Application object's DispatcherUnhandledException event.
                ApplicationDispatcher.Invoke(
                        DispatcherPriority.Send,
                       (DispatcherOperationCallback)(arg => { throw new AggregateException(exception); }),
                       null);
            }
        }

        /// <summary>
        /// Function to log error details.
        /// </summary>
        /// <param name="exception">Exception information</param>
        public static void Log(this Exception exception)
        {
        }

        #endregion
    }
}