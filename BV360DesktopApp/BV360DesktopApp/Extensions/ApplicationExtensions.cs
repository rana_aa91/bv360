﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace BV360DesktopApp
{

    /// <summary>
    /// This class contains extension methods for Application.
    /// </summary>
    public static class ApplicationExtensions
    {
        #region Public Methods

        /// <summary>
        /// Function to close all windows except type of windows given.
        /// </summary>
        /// <param name="application">Instance of Application</param>
        public static void CloseOpenDialogs(this Application application)
        {
            // Application.Current will be null when error occur during shutdown
            if (application == null)
            {
                return;
            }

            foreach (Window window in application.Windows)
            {
                if (application.MainWindow != window)
                {
                    window.Close();
                }
            }
        }

        /// <summary>
        /// Function to check if any application window is visible.
        /// </summary>
        /// <param name="application">Instance of Application</param>
        /// <returns>True if any application window is visible or not</returns>
        public static bool IsAnyWindowVisible(this Application application)
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            foreach (Window window in application.Windows)
            {
                if (window.IsVisible)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Function to assign cursor to current window.
        /// </summary>
        /// <param name="application">Instance of Application</param>
        public static void AssignCursor(this Application application)
        {
            if (application != null && application.MainWindow != null)
            {
                application.MainWindow.Cursor = Cursors.Arrow;
            }
        }

        /// <summary>
        /// Function to assign cursor to current window.
        /// </summary>
        /// <param name="application">Instance of Application</param>
        public static void ShutdownWhenNoWindowVisible(this Application application)
        {
            if (application != null && !application.IsAnyWindowVisible())
            {
                application.Shutdown();
            }
        }

        /// <summary>
        /// Function to show window as dialog.
        /// </summary>
        /// <typeparam name="T">The Window type</typeparam>
        /// <param name="application">Instance of Application</param>
        /// <param name="windowFunc">Function to retrieve child window instance</param>
        public static void ShowWindowAsDialog<T>(this Application application, Func<T> windowFunc) where T : Window
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            if (windowFunc == null)
            {
                throw new ArgumentNullException("windowFunc");
            }

            if (application.Windows.Count > 0)
            {
                Window window = application.Windows.OfType<T>().FirstOrDefault();
                if (window == null)
                {
                    application.OpenWindowAsDialog<T>(windowFunc, true);
                }
                else
                {
                    window.Activate();
                }
            }
        }

        /// <summary>
        /// Function to open window as dialog.
        /// </summary>
        /// <typeparam name="T">The Window type</typeparam>
        /// <param name="application">Instance of Application</param>
        /// <param name="windowFunc">Function to retrieve child window instance</param>
        /// <param name="isMask">Value indicating whether to mask parent or not</param>
        public static void OpenWindowAsDialog<T>(this Application application, Func<T> windowFunc, bool isMask) where T : Window
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            if (windowFunc == null)
            {
                throw new ArgumentNullException("windowFunc");
            }

            var parentWindow = application.Windows[Application.Current.Windows.Count - 1];

            Window window = windowFunc();
            window.Owner = parentWindow;
            window.ShowDialog();
            window.Owner = null;
        }

        #endregion
    }
}