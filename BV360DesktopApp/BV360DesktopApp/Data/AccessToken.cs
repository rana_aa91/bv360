﻿using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace BV360DesktopApp
{
    public class AccessToken
    {

        public string Token { get; set; }
        //public DateTime CreationDate { get; }

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly double maxWaitTimeInSeconds = 3600 - (10 * 60); //refresh when time left is 10 min to token expiration
        //private string token;
        private DateTime creationDate;

        //Token Manager - Needs to be moved somewhere else - Same code exists in UploadHandler, HashHandler and SiteHandler
        public async Task GetTokenAsync()
        {
            var client = new HttpClient();
            var req = new ClientCredentialsTokenRequest
            {
                Address = APIHandler.GET_TOKEN,
                ClientId = APIHandler.CLIENT_ID,
                ClientSecret = APIHandler.CLIENT_SECRET,
                Scope = "Developer AuthorisedApp",
            };

            var response = await client.RequestClientCredentialsTokenAsync(req);

            if (response.IsError)
                Token = null;
            creationDate = DateTime.UtcNow;
            Token = response.AccessToken;
        }

        public async Task CheckToken()
        {
            if ((DateTime.UtcNow - creationDate).TotalSeconds > maxWaitTimeInSeconds)
            {
                await GetTokenAsync();
                creationDate = DateTime.UtcNow;
                log.Debug("Got a new token...");
            }
        }
    }
}
