﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using IdentityModel.Client;
using log4net;

namespace BV360DesktopApp
{
    public class UploadHandler
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int numberOfRetries = 3;
        private FileItem file;
        private SiteItem site;
        private AccessToken _AccessToken = new AccessToken();       
        private string uploadID;
        
        
        private FileStream fstream;
        private int chunkSize;
        private int chunkLimit;

        public int ChunkNumInProgress { get; set; } = -1;
        public int TotalChunkNum { get; set; }
        public bool UploadFailed = false;

        public UploadHandler(FileItem f, SiteItem s)
        {
            file = f;
            site = s;
            fstream = new FileStream(file.Path, FileMode.Open, FileAccess.Read);
        }

        /// <summary>
        /// Sets all parameters necessary to upload file
        /// </summary>
        /// <returns>Returns true if upload setup was successful</returns>
        public async Task<bool> UploadSetup()
        {
            try
            {
                //Get Token               
                await _AccessToken.GetTokenAsync(); //start timer ?
                //Get File UploadLimitations
                chunkSize = await GetChunkSizeAsync();
                chunkLimit = await GetChunkLimitAsync();
                //Get File Details - for Chunk Upload
                SetupUploadChunksAsync(file.Path, chunkSize, uploadID);
                //Get Upload ID
                return await CreateUploadSessionAsync();
            }
            catch (Exception ex)
            {
                ex.Log();
                return true;
            }
        }

        /// <summary>
        /// Creates an upload session - with a unique ID
        /// </summary>
        /// <returns>Returns positive if upload session was created successfully</returns>
        private async Task<bool> CreateUploadSessionAsync()
        {
            await _AccessToken.CheckToken();

            //Create UploadInfo object
            UploadInfo obj = new UploadInfo(file, site);

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("authorization", "Bearer " + _AccessToken.Token);
            var response = await httpClient.PostAsync(APIHandler.CREATE_UPLOADSESSION_API, new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json"));
            string result = await response.Content.ReadAsStringAsync();
            log.Debug($"Creating Upload Session Result : {result}, response success {response.IsSuccessStatusCode.ToString()}");
            if (response.IsSuccessStatusCode)
            {
                uploadID = JsonConvert.DeserializeObject<UploadSessionInfo>(result).id;
                log.Debug($"Upload {uploadID} created.");
                return true;
            }
            return false; //failed to create upload session
        }

        private void SetupUploadChunksAsync(string name, int chunkSize, string uploadID)
        {
            long length = new FileInfo(name).Length;
            TotalChunkNum = (int)(length / chunkSize);
            if (length % chunkSize != 0)
                TotalChunkNum++;

            if (TotalChunkNum < 1) //for small files
            {
                chunkSize = (int)length;
                TotalChunkNum++;
            }
        }

        /// <summary>
        /// Uploads next buffered chunk 
        /// </summary>
        /// <returns>Returns true if the chunk upload was successful</returns>
        public async Task<bool> UploadNextChunkAsync()
        {
            int i = 0;
            while (i < numberOfRetries)
            {
                if (ChunkNumInProgress < TotalChunkNum)
                    ChunkNumInProgress++;
                else
                {
                    //no more chunks to upload
                    log.Debug("No more chunks to upload - this shouldn't happen!");
                    return false;
                }
                try
                {                    

                    await _AccessToken.CheckToken();

                    long position = (ChunkNumInProgress * (long)chunkSize);
                    log.Debug($"{ file.Name}: Chunk #{ChunkNumInProgress}, Try {i.ToString()}/{numberOfRetries.ToString()}, Position = {position.ToString()}");
                    int toRead = (int)Math.Min(file.Length - position + 1, chunkSize);
                    log.Debug($"{ file.Name}: Chunk #{ChunkNumInProgress}, Try {i.ToString()}/{numberOfRetries.ToString()}, toRead = {toRead.ToString()}");                   
                    byte[] buffer = new byte[toRead];
                    int bytesRead = await fstream.ReadAsync(buffer, 0, buffer.Length);
                    log.Debug($"{ file.Name}: Chunk #{ChunkNumInProgress}, Try {i.ToString()}/{numberOfRetries.ToString()}, ByteToRead = {bytesRead.ToString()}");
                    if (bytesRead == 0)
                    {
                        log.Debug("No more bytes to read - this shouldn't happen!");
                        return false;
                    }
                        
                    MultipartFormDataContent form = new MultipartFormDataContent()
                {
                    {new StreamContent(new MemoryStream(buffer)), "Chunk", Path.GetFileName(file.Name)},
                    {new StringContent(uploadID), "UploadID"},
                    {new StringContent($"{ChunkNumInProgress}"), "ChunkNum"},

                };

                    //Stage 3: Upload Chunks
                    HttpClient httpClient = new HttpClient();
                    httpClient.DefaultRequestHeaders.Add("authorization", "Bearer " + _AccessToken.Token);
                    var response = await httpClient.PostAsync(APIHandler.POST_CHUNK_API, form).ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                        return true;
                    i++;
                    log.Debug($"{file.Name}: Chunk #{ChunkNumInProgress}, Try {i.ToString()}/{numberOfRetries.ToString()}, failed with message: {response.RequestMessage}");
                   
                    
                }
                catch (Exception ex)
                {
                    i++;
                    log.Debug($"{file.Name}: Chunk #{ChunkNumInProgress}, Try {i.ToString()}/{numberOfRetries.ToString()}, failed with exception message: {ex.Message.ToString()}");
                    
                }
            }

            log.Debug($"{file.Name}: Chunk #{ChunkNumInProgress}, failed at all attempts.");

            return false;
            
        }
        
        /// <summary>
        /// Sends command to finish upload session
        /// </summary>
        /// <returns>Returns true if upload sesssion was terminated successfully</returns>
        public async Task<bool> UploadFinishAsync()
        {
            await _AccessToken.CheckToken();

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("authorization", "Bearer " + _AccessToken.Token);
            string url = APIHandler.FINISH_UPLOADSESSION_API(uploadID);
            var response = await httpClient.PostAsync(url, new StringContent(uploadID));
            return response.IsSuccessStatusCode;
        }

        private async Task<int> GetChunkSizeAsync()
        {
            await _AccessToken.CheckToken(); //always check token before using

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("authorization", "Bearer " + _AccessToken.Token);
            HttpResponseMessage response = await httpClient.GetAsync(APIHandler.GET_CHUNKSIZE_API);
            string result = response.Content.ReadAsStringAsync().Result;
            log.Debug($"Getting Chunk Size Result : {result}, response success {response.IsSuccessStatusCode.ToString()}");
            if (response.IsSuccessStatusCode)
                return int.Parse(result);
            return 0;
        }

        private async Task<int> GetChunkLimitAsync()
        {
            await _AccessToken.CheckToken();

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("authorization", "Bearer " + _AccessToken.Token);
            HttpResponseMessage response = await httpClient.GetAsync(APIHandler.GET_CHUNKLIMIT_API);
            string result = response.Content.ReadAsStringAsync().Result;
            log.Debug($"Getting Chunk Limit Result : {result}, response success {response.IsSuccessStatusCode.ToString()}");
            if (response.IsSuccessStatusCode)
                return int.Parse(result);
            return 0;
        }

        private async Task<int> GetByteLimitAsync()
        {
            await _AccessToken.CheckToken();

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("authorization", "Bearer " + _AccessToken.Token);
            HttpResponseMessage response = await httpClient.GetAsync(APIHandler.GET_BYTELIMIT_API);
            string result = response.Content.ReadAsStringAsync().Result;
            log.Debug($"Getting Byte Limit Result : {result}, response success {response.IsSuccessStatusCode.ToString()}");
            if (response.IsSuccessStatusCode)
                return int.Parse(result);
            return 0;
        }

        
    }


}

