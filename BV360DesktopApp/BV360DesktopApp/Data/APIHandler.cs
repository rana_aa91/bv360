﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BV360DesktopApp
{
    static class APIHandler
    {
        //store all the urls here temporarily
        //might need to move later to AppSettings

        private const string MAIN_URL = "https://builtview360-backend-dev.azurewebsites.net";

        

        //Authorisation 
        static public readonly string GET_TOKEN = MAIN_URL+"/connect/token";
        static public readonly string CLIENT_ID = "win-sync";
        static public readonly string CLIENT_SECRET = "hMSA9dqRbBcBCPYmzCwxsjJD2DY3gfH4Vwb3NMUubaap7aCvG7E8ryNffvAK";

        //Get APIs
        static public readonly string GET_ALLSITES_API = MAIN_URL+ "/api/sites";
        static public readonly string GET_CHUNKSIZE_API = MAIN_URL + "/api/uploads/limits/chunk-bytes";
        static public readonly string GET_CHUNKLIMIT_API = MAIN_URL + "/api/uploads/limits/max-chunks";
        static public readonly string GET_BYTELIMIT_API = MAIN_URL + "/api/uploads/limits/max-bytes";
        static public string GET_UPLOAD_HASH_API(string hash)
        {
            return $"{MAIN_URL}/api/uploads/hash/{hash}";

        }
        //Post APIs
        static public readonly string POST_CHUNK_API = MAIN_URL + "/api/uploads/chunks/any";
        static public readonly string CREATE_UPLOADSESSION_API = MAIN_URL + "/api/uploads/any";
        static public string FINISH_UPLOADSESSION_API(string id)
        {
            return  $"{MAIN_URL}/api/uploads/{id}/finish/any";
        }


    }
}
