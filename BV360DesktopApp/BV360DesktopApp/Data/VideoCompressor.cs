﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace BV360DesktopApp
{
    public static class Ffmpeg
    {         
  
        public static void Compress()
        {
            try
            {
                string workingDirectory = Directory.GetCurrentDirectory();
                string fileDirectory = @"C:\Users\Rana\Documents\code\BV360\BV360DesktopApp\BV360DesktopApp\input.mp4";

                //p = new NamedPipeServerStream(pipename, PipeDirection.Out, 1, PipeTransmissionMode.Byte);

                ProcessStartInfo psi = new ProcessStartInfo();
                psi.WindowStyle = ProcessWindowStyle.Normal;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = false;
                psi.ErrorDialog = true;
                psi.FileName = @"C:\Users\Rana\Documents\code\BV360\BV360DesktopApp\ffmpeg\bin\ffmpeg.exe";
                psi.WorkingDirectory = @"C:\Users\Rana\Documents\code\BV360\BV360DesktopApp\ffmpeg\bin\";
                psi.Arguments = "-f -i "+fileDirectory+" -c:v libx264 -crf 22 -c:a copy output.mp4";
                Process process = Process.Start(psi);
                process.EnableRaisingEvents = false;
                psi.RedirectStandardError = true;
               // p.WaitForConnection();
            }
            catch (Exception err)
            {
                //Logger.Write("Exception Error: " + err.ToString());
            }
        }
        
    }
}