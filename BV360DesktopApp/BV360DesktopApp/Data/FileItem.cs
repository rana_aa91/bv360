﻿using System;
using System.Collections.Generic;

namespace BV360DesktopApp
{
    public class FileItem : NotificationBase
    {

        //Intrinsic file-related properties      
        public string Name { get; set; }
        public string Path { get; set; }
        public string FileExtension { get; set; } //for now we only support mp4
        public DateTime CreationDateTime { get; set; }

        public long Length { get; set; }
        public string Size
        {
            get { return LengthToSize(Length); }
        }

        public string HashString
        {
            get { return HashHandler.GetHashString(Name + CreationDateTime.ToString() + Length); }
        }
        //User-Added Properties
        public string Description
        {
            get { return this.description; }
            set { this.SetProperty(() => this.Description, () => this.description, value); }
        }
        
        public IList<string> Tags
        {
            get { return this.tags; }
            set { this.tags = value; }
        }

        public int Index
        {
            get { return this.index; }
            set { this.SetProperty(() => this.Index, () => this.index, value); }
        }

        public bool IsSelected
        {
            get { return this.isSelected; }
            set { this.SetProperty(() => this.IsSelected, () => this.isSelected, value); }
        }

        public bool CompressSelected
        {
            get { return this.compressSelected; }
            set { this.SetProperty(() => this.CompressSelected, () => this.compressSelected, value); }
        }
        //Upload related properties
        public FileStatus FileStatus
        {
            get { return this.fileStatus; }
            set { this.SetProperty(() => this.FileStatus, () => this.fileStatus, value); }
        }
        //add one for percentage of upload...       
        public string ChunkPercentageInProgress
        {
            get { return this.chunkPercentageInProgress; }
            set { this.SetProperty(() => this.ChunkPercentageInProgress, () => this.chunkPercentageInProgress, value); }
        }

        //need to relocate
        private int index;
        private bool isSelected;
        private bool compressSelected = false;
        private string description = " ";
        private string chunkPercentageInProgress = "0 %";
        private FileStatus fileStatus = BV360DesktopApp.FileStatus.None;
        private IList<string> tags = new List<string>();
        private string LengthToSize(long len)
        {
            string[] sizes = { "B", "KB", "MB", "GB", "TB" };
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            return String.Format("{0:0.##} {1}", len, sizes[order]);
        }

        
    }
}