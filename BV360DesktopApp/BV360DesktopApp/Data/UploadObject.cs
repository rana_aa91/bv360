﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnedriveGraphTest
{
    public class UploadObject
    {
        public string siteId = "aa523604-8b1a-4981-bfc7-122f1a830ee7"; //enexg
        public string fileName;
        public int byteCount ;
        public string captureDate = "2020-06-03T04:48:02+10:00";
        public string hash = "000";
        public string description = "RANA APP";
        public string[] tags = { };
        public string hardLink = " ";
        public string hardThumbnailLink = " ";

        public UploadObject(string file)
        {
            fileName = Path.GetFileName(file); 
            byteCount = (int)(new FileInfo(file).Length);
        }

        
}
}
