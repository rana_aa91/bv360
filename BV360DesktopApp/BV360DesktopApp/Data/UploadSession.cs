﻿using System;
using System.Collections.ObjectModel;

namespace BV360DesktopApp
{
    public class UploadSession
    {     

        public SiteItem Site { get; set; }

        /// <summary>
        /// Gets or set session start date time.
        /// </summary>
        public DateTime? StartDateTime { get; set; }

        /// <summary>
        /// Gets or set session end date time.
        /// </summary>
        public DateTime? EndDateTime { get; set; }

        /// <summary>
        /// Gets or sets list of files to be uploaded.
        /// </summary>
        public ObservableCollection<FileItem> FileList
        {
            get { return this.fileList; }

            set { this.fileList = value; }
        }

        private ObservableCollection<FileItem> fileList = new ObservableCollection<FileItem>();
    }
}