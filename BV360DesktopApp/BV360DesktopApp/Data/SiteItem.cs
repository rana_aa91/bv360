﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BV360DesktopApp
{
    public class SiteItem 
    {

        public string SiteId { get; set; }
       
        public string SiteName { get; set; }

        public string Shorthand { get; set; }
    }
    
}
