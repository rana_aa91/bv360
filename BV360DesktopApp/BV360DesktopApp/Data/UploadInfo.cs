﻿
using System.Collections.Generic;
using System.Text;



namespace BV360DesktopApp
{
    public class UploadInfo
    {
        public UploadInfo(FileItem file, SiteItem s)
        {
            fileName = file.Name;
            byteCount = (int)(file.Length);
            captureDate = file.CreationDateTime.ToString("yyyy-MM-ddTHH:mm:sszzz"); //date yyyy-MM-ddTHH:mm:sszzz //example: 2020-05-19T12:47:20+10:00
            hash = file.HashString;

            description = file.Description;
            foreach (string tag in file.Tags)
                tags.Add(tag);

            siteId = s.SiteId;


        }

        public string fileName;
        public int byteCount;
        public string captureDate;
        public string hash = "000";
        public string description = " ";
        public List<string> tags = new List<string>();
        public string hardLink = " ";
        public string hardThumbnailLink = " ";
        public string siteId;


       

       

    }
}
