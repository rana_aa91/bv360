﻿using System.Net.Http;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using IdentityModel.Client;
using System;

namespace BV360DesktopApp
{
    public static class HashHandler
    {
        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static async Task<bool> HashExists(string hash)
        {
            AccessToken _AccessToken = new AccessToken() ; //start timer ?
            await _AccessToken.GetTokenAsync();

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("authorization", "Bearer " + _AccessToken.Token);
            HttpResponseMessage response = await httpClient.GetAsync(APIHandler.GET_UPLOAD_HASH_API(hash));
            if(response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                if (result.Length > 2) //not the best condition, if empty result = "[]"
                    return true;
            }
            
            return false;
            
        }
        private static byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

      
    }
}
