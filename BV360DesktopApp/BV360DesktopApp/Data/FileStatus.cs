﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BV360DesktopApp
{
    public enum FileStatus
    {
        // There is no operation being performed.
        None,

        // In queue 
        Pending,

        //File upload session being setup
        Setup,

        // File upload is in progress.
        CompressionInProgress,

        // File upload is in progress.
        UploadInProgress,

        // File already exists on storage.
        AlreadyExists,

        // upload file operation is complete.
        Complete,

        // Error occurred while uploading.
        Error
    }
}
