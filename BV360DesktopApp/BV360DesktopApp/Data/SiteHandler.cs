﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BV360DesktopApp
{
    public static class SiteHandler
    {
        public static async Task<List<SiteItem>> GetAllSitesAsync()
        {
            List<SiteItem> SiteList = new List<SiteItem>();

            AccessToken _AccessToken = new AccessToken(); //start timer ?
            await _AccessToken.GetTokenAsync();

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("authorization", "Bearer " + _AccessToken.Token);
            HttpResponseMessage response = await httpClient.GetAsync(APIHandler.GET_ALLSITES_API);

            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                return null;
            }
            else
            {
                //if it failed,return some hard-coded values 
                SiteItem etp = new SiteItem
                {
                    SiteId = "47cfc0e2-aa80-479e-8cff-e90a06d802a8",
                    SiteName = "Engineering Technology Precinct ",
                    Shorthand = "etp"
                };
                SiteItem enexg = new SiteItem
                {
                    SiteId = "aa523604-8b1a-4981-bfc7-122f1a830ee7",
                    SiteName = "Engineering Excellence",
                    Shorthand = "enexg"
                };
                SiteItem csm = new SiteItem
                {
                    SiteId = "f68e2a5e-1c3e-45ed-972e-74924dbe3542",
                    SiteName = "Central Station Metro",
                    Shorthand = "csm"
                };

                SiteList.Add(etp);
                SiteList.Add(enexg);
                SiteList.Add(csm);
                return SiteList;
            }
        }
    }
}


