﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace BV360DesktopApp
{
    [TemplatePart(Name = "PART_CreateTagButton", Type = typeof(Button))]
    public class VideoTagControl : ListBox
    {
        public event EventHandler<VideoTagEventArgs> TagClick;
        public event EventHandler<VideoTagEventArgs> TagAdded;
        public event EventHandler<VideoTagEventArgs> TagRemoved;

        static VideoTagControl()
        {
            // lookless control, get default style from generic.xaml
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VideoTagControl), new FrameworkPropertyMetadata(typeof(VideoTagControl)));
        }

        public VideoTagControl()
        {
            //this is what is displayed when window first opens up - needs to be recovered from history
            //this.ItemsSource = new List<VideoTagItem>() { new VideoTagItem("slab"), new VideoTagItem("underground") }; 

            //hard-coded values for autocomplete
            this.AllTags = new List<string>() { "CSM","ETP","NS Concourse", "Metro Box", "Level 1", "Level 2","Level 3","Level 4", "Level 5", "Level 6", "Level 7", "Level 8", "Level 9", "Level 10" }; 
        }

        // AllTags
        public List<string> AllTags { get { return (List<string>)GetValue(AllTagsProperty); } set { SetValue(AllTagsProperty, value); } }
        public static readonly DependencyProperty AllTagsProperty = DependencyProperty.Register("AllTags", typeof(List<string>), typeof(VideoTagControl), new PropertyMetadata(new List<string>()));


        // IsEditing, readonly
        public bool IsEditing { get { return (bool)GetValue(IsEditingProperty); } internal set { SetValue(IsEditingPropertyKey, value); } }
        private static readonly DependencyPropertyKey IsEditingPropertyKey = DependencyProperty.RegisterReadOnly("IsEditing", typeof(bool), typeof(VideoTagControl), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty IsEditingProperty = IsEditingPropertyKey.DependencyProperty;

        public override void OnApplyTemplate()
        {
            Button createBtn = this.GetTemplateChild("PART_CreateTagButton") as Button;
            if (createBtn != null)
                createBtn.Click += createBtn_Click;

            base.OnApplyTemplate();
        }

        void createBtn_Click(object sender, RoutedEventArgs e)
        {
            var newItem = new VideoTagItem() { IsEditing = true };
            AddTag(newItem);
            this.SelectedItem = newItem;
            this.IsEditing = true;

        }

        /// <summary>
        /// Adds a tag to the collection
        /// </summary>
        internal void AddTag(VideoTagItem tag)
        {
            if (this.ItemsSource == null)
                this.ItemsSource = new List<VideoTagItem>();

            ((IList)this.ItemsSource).Add(tag); // assume IList for convenience
            this.Items.Refresh();

            if (TagAdded != null)
                TagAdded(this, new VideoTagEventArgs(tag));
        }

        /// <summary>
        /// Removes a tag from the collection
        /// </summary>
        internal void RemoveTag(VideoTagItem tag, bool cancelEvent = false)
        {
            if (this.ItemsSource != null)
            {
                ((IList)this.ItemsSource).Remove(tag); // assume IList for convenience
                this.Items.Refresh();

                if (TagRemoved != null && !cancelEvent)
                    TagRemoved(this, new VideoTagEventArgs(tag));
            }
        }


        /// <summary>
        /// Raises the TagClick event
        /// </summary>
        internal void RaiseTagClick(VideoTagItem tag)
        {
            if (this.TagClick != null)
                TagClick(this, new VideoTagEventArgs(tag));
        }
    }
}