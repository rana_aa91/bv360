﻿// <copyright file="SpinWheelUserControl.xaml.cs" company="">Copyright (C) 2020</copyright>

namespace BV360DesktopApp.Controls
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Animation;

    /// <summary>
    /// Interaction logic for SpinWheelUserControl.xaml
    /// </summary>
    public partial class SpinWheelUserControl : UserControl
    {
        #region Private Members
        /// <summary>
        /// Using a DependencyProperty as the backing store for line color
        /// </summary>
        private static readonly DependencyProperty LineColorProperty = DependencyProperty.Register("LineColor", typeof(Brush), typeof(SpinWheelUserControl), new UIPropertyMetadata(Brushes.Black));

        /// <summary>
        /// Using a DependencyProperty as the backing store for AllowMasking
        /// </summary>
        private static readonly DependencyProperty AllowMaskingProperty = DependencyProperty.Register("AllowMasking", typeof(bool), typeof(SpinWheelUserControl), new UIPropertyMetadata(false));

        /// <summary>
        /// Using a DependencyProperty as the backing store for MaskingBackgroundColor
        /// </summary>
        private static readonly DependencyProperty MaskingBackgroundColorProperty = DependencyProperty.Register("MaskingBackgroundColor", typeof(Brush), typeof(SpinWheelUserControl), new UIPropertyMetadata(Brushes.Transparent));

        /// <summary>
        /// Using a DependencyProperty as the backing store for MaskingOpacity
        /// </summary>
        private static readonly DependencyProperty MaskingOpacityProperty = DependencyProperty.Register("MaskingOpacity", typeof(double), typeof(SpinWheelUserControl), new UIPropertyMetadata(1.0));

        /// <summary>
        /// Using a DependencyProperty as the backing store for SpinWheelBackgroundColor
        /// </summary>
        private static readonly DependencyProperty SpinWheelBackgroundColorProperty = DependencyProperty.Register("SpinWheelBackgroundColor", typeof(Brush), typeof(SpinWheelUserControl), new UIPropertyMetadata(Brushes.Transparent));

        /// <summary>
        /// Using a DependencyProperty as the backing store for SpinWheelBackgroundBorderColor
        /// </summary>
        private static readonly DependencyProperty SpinWheelBackgroundBorderColorProperty = DependencyProperty.Register("SpinWheelBackgroundBorderColor", typeof(Brush), typeof(SpinWheelUserControl), new UIPropertyMetadata(Brushes.Transparent));

        /// <summary>
        /// Using a DependencyProperty as the backing store for SpinWheelBackgroundBorderThickness
        /// </summary>
        private static readonly DependencyProperty SpinWheelBackgroundBorderThicknessProperty = DependencyProperty.Register("SpinWheelBackgroundBorderThickness", typeof(Thickness), typeof(SpinWheelUserControl), new UIPropertyMetadata(new Thickness(1)));

        /// <summary>
        /// Constant for spin wheel story board
        /// </summary>
        private const string SpinWheelAnimationStoryboard = "spinWheelAnimationStoryboard";

        /// <summary>
        /// Holds spin wheel animation story board
        /// </summary>
        private Storyboard spinWheelAnimationStoryboard;
        #endregion

        public SpinWheelUserControl()
        {
            InitializeComponent();
            this.spinWheelAnimationStoryboard = (Storyboard)this.Resources[SpinWheelAnimationStoryboard];
        }

        #region Public Members

        /// <summary>
        /// Gets or sets spin wheel color
        /// </summary>
        public Brush Color
        {
            get { return (Brush)GetValue(LineColorProperty); }
            set { SetValue(LineColorProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether masking is allowed.
        /// </summary>
        public bool AllowMasking
        {
            get { return (bool)GetValue(AllowMaskingProperty); }
            set { SetValue(AllowMaskingProperty, value); }
        }

        /// <summary>
        /// Gets or sets color of masking background.
        /// </summary>
        public Brush MaskingBackgroundColor
        {
            get { return (Brush)GetValue(MaskingBackgroundColorProperty); }
            set { SetValue(MaskingBackgroundColorProperty, value); }
        }

        /// <summary>
        /// Gets or sets opacity of masking.
        /// </summary>
        public double MaskingOpacity
        {
            get { return (double)GetValue(MaskingOpacityProperty); }
            set { SetValue(MaskingOpacityProperty, value); }
        }

        /// <summary>
        /// Gets or sets color of background of spin wheel.
        /// </summary>
        public Brush SpinWheelBackgroundColor
        {
            get { return (Brush)GetValue(SpinWheelBackgroundColorProperty); }
            set { SetValue(SpinWheelBackgroundColorProperty, value); }
        }

        /// <summary>
        /// Gets or sets color of border of background of spin wheel.
        /// </summary>
        public Brush SpinWheelBackgroundBorderColor
        {
            get { return (Brush)GetValue(SpinWheelBackgroundBorderColorProperty); }
            set { SetValue(SpinWheelBackgroundBorderColorProperty, value); }
        }

        /// <summary>
        /// Gets or sets color of border thickness of background of spin wheel.
        /// </summary>
        public Thickness SpinWheelBackgroundBorderThickness
        {
            get { return (Thickness)GetValue(SpinWheelBackgroundBorderThicknessProperty); }
            set { SetValue(SpinWheelBackgroundBorderThicknessProperty, value); }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Function to begin animation
        /// </summary>
        private void StartAnimation()
        {
            this.spinWheelAnimationStoryboard.Begin(this.spinWheel, true);
        }

        /// <summary>
        /// Function to stop animation
        /// </summary>
        private void StopAnimation()
        {
            this.spinWheelAnimationStoryboard.Stop(this.spinWheel);
        }

        /// <summary>
        /// Event to start or stop spin wheel animation
        /// </summary>
        /// <param name="sender">The one who raises the event</param>
        /// <param name="e">Holds information related to the event</param>
        private void SpinWheelUserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.Visibility == Visibility.Visible)
            {
                this.StartAnimation();
            }
            else
            {
                this.StopAnimation();
            }
        }
        #endregion
    }
}
