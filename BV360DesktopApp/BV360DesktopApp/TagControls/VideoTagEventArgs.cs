﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BV360DesktopApp
{


    public class VideoTagEventArgs : EventArgs
    {
        public VideoTagItem Item { get; set; }

        public VideoTagEventArgs(VideoTagItem item)
        {
            this.Item = item;
        }
    }
}
